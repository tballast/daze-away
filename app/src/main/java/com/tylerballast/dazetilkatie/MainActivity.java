package com.tylerballast.dazetilkatie;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.text.Html;
import android.util.Log ;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.mikhaellopez.circularimageview.CircularImageView;

import org.joda.time.DateTime;
import org.joda.time.Days;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;


import java.util.Calendar;

import java.util.Random;

//import com.pkmmte.view.CircularImageView;

public class MainActivity extends AppCompatActivity {

    public final static String EXTRA_MESSAGE = "com.mycompany.myfirstapp.MESSAGE";

    CircularImageView image;
    TextView daysUntilView ;
    TextView dateView;
    int currentImageId ;
//    TextView weeksView;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;
    final Calendar c = Calendar.getInstance() ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Display an Image
        addListenerOnImage();

//        initializeDaysUntilView() ;
        // Set default text
//        TextView newtext = (TextView) findViewById(R.id.dateDisplay);
//        newtext.setText(DateFormat.getDateInstance().format(new Date()));


        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

//    @Override
//    protected void onPause()
//    {
//        // Get the current date string from the textview
//        TextView dateView = (TextView) findViewById(R.id.dateDisplay) ;
//        String dateString = dateView.getText().toString() ;
//
//        SharedPreferences sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
//        SharedPreferences.Editor editor = sharedPref.edit();
//        editor.putString(getString(R.string.select_date_text), dateString);
//        editor.apply();
//
//
//
//    }


    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

            c.set(Calendar.YEAR, year);
            c.set(Calendar.MONTH, monthOfYear) ;
            c.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            setDaysFromOnView() ;
        }
    };

    public void dateOnClick( View view )
    {
        // This will call a date picker dialog
        new DatePickerDialog(this, date, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH)).show() ;


    }

    public void addListenerOnImage() {
        image = (CircularImageView) findViewById(R.id.imageView1);

        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

                TypedArray images_array = getResources().obtainTypedArray(R.array.randImgs);
                final Random rand = new Random();
                final int rndInt = rand.nextInt(images_array.length());
                currentImageId = images_array.getResourceId(rndInt, 0) ;
                image.setImageResource(currentImageId);
                images_array.recycle();
            }
        });
    }

    public void daysOnClick( View view )
    {

//        // TODO Need to keep track of the current view so that if you go and change the date while in a particular view it will hold that view
//        Log.d("DateApp", "daysOnClick Called") ;
//
//        daysUntilView = (TextView) findViewById(R.id.daysRemaining) ;
//        dateView = (TextView) findViewById(R.id.dateDisplay) ;
//
//        DateTime dateFuture = new DateTime(c) ;
//        DateTime dateCurrent = new DateTime() ;
//
//        int daysBetween = Days.daysBetween(dateCurrent, dateFuture).getDays() ;
//
//        double dweeks = Math.floor(daysBetween / 7.0) ;
//        int daysRemainder = daysBetween - (int)dweeks * 7 ;
//        int weeks = (int)dweeks ;
//        daysUntilView.setText(Html.fromHtml(String.valueOf(weeks) + "<sup><small><small>w</small></small></sup>" + String.valueOf(daysRemainder) + "<sup><small>d</small></sup>"));


    }

    private void setDaysFromOnView()
    {
        // Finds days between current day and the day specified in the calendar C

        daysUntilView = (TextView) findViewById(R.id.daysRemaining) ;
        dateView = (TextView) findViewById(R.id.dateDisplay) ;
//        weeksView = (TextView) findViewById(R.id.daysAndWeeksRemaining) ;

        DateTime dateFuture = new DateTime(c) ;
        DateTime dateCurrent = new DateTime() ;

        int daysBetween = Days.daysBetween(dateCurrent, dateFuture).getDays() ;
        daysUntilView.setText(String.valueOf(daysBetween)) ;
        double dweeks = Math.floor(daysBetween / 7.0) ;
        int daysRemainder = daysBetween - (int)dweeks * 7 ;
        int weeks = (int)dweeks ;


        String daysAndWeeks = String.valueOf(weeks) + " weeks, " + String.valueOf(daysRemainder) + " days";

        DateTimeFormatter fmt = DateTimeFormat.forPattern("MMM dd, yyyy") ;
        String formattedDate = dateFuture.toString( fmt ) ;
        dateView.setText(formattedDate);

        Log.d("DateApp", "set days between: " + daysBetween) ;

//        // Saving Data
//        SharedPreferences sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
//        SharedPreferences.Editor editor = sharedPref.edit();
//        editor.putInt(getString(R.string.saved_high_score), newHighScore);
//        editor.commit();
//
//        //Loading of the saved data
//        SharedPreferences sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
//        int defaultValue = getResources().getInteger(R.string.saved_high_score_default);
//        long highScore = sharedPref.getInt(getString(R.string.saved_high_score), defaultValue);
    }

    @Override
    public void onStart() {
        super.onStart();

        SharedPreferences sharedPref = this.getPreferences(Context.MODE_PRIVATE);

        String default_days = getResources().getString(R.string.days);
        String retrieved_days = sharedPref.getString(getString(R.string.days), default_days);

        String defaultValue = getResources().getString(R.string.date_text);
        String retrieved_date = sharedPref.getString(getString(R.string.date_text), defaultValue);


        TypedArray images_array = getResources().obtainTypedArray(R.array.randImgs);
        final Random rand = new Random();
        final int rndInt = rand.nextInt(images_array.length());
        int randID = images_array.getResourceId(rndInt, 0) ;
        currentImageId = sharedPref.getInt("currImage", randID) ;
        image.setImageResource(currentImageId);
        images_array.recycle();



        if( !retrieved_date.equals("Select Date") )
        {
            DateTimeFormatter format =  DateTimeFormat.forPattern("MMM dd, yyyy") ;
            DateTime dt_future = format.parseDateTime(retrieved_date) ;

            DateTime dt_current = new DateTime() ;
            int daysBetween  ;
            try {
                daysBetween = Days.daysBetween(dt_current, dt_future).getDays();
            }
            catch(IllegalArgumentException e)
            {
                daysBetween = 0 ;
                retrieved_date = "Select Date" ;
            }
            retrieved_days = String.valueOf(daysBetween) ;
        }

        TextView newtext = (TextView) findViewById(R.id.dateDisplay);
        newtext.setText(retrieved_date);

        TextView daystext = (TextView) findViewById(R.id.daysRemaining);
        daystext.setText(retrieved_days);
//        setDaysFromOnView() ;
        // Now need to update days until.

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Main Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.tylerballast.dazetilkatie/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // Get the current date string from the textview
        TextView dateView = (TextView) findViewById(R.id.dateDisplay) ;
        String dateString = dateView.getText().toString() ;

        TextView daysView = (TextView) findViewById(R.id.daysRemaining) ;
        String daysString = daysView.getText().toString() ;


//        CircularImageView cview = (CircularImageView) findViewById(R.id.imageView1) ;
//        Drawable cdraw = cview.getDrawable() ;

        SharedPreferences sharedPref = this.getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(getString(R.string.date_text), dateString);
        editor.putString(getString(R.string.days), daysString);
        editor.putInt("currImage", currentImageId) ;
        editor.apply();


        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Main Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.tylerballast.dazetilkatie/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }

}
